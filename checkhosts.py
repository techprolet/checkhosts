#!/usr/bin/python
'''
Created on Mar 8, 2014

@author: Pavlos Iliopoulos
http://www.techprolet.com/

using dnspython library,
available at https://github.com/rthalley/dnspython

USAGE:
python checkhosts.py ./hosts

    This file is part of "CheckHosts".

    "CheckHosts" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "CheckHosts".  If not, see <http://www.gnu.org/licenses/>.
-->
'''
import sys
import dns
import dns.resolver
from sets import Set


def checkHosts(hostsFileName):
    print "checking file:"+hostsFileName
#   load files
    hostsFile = open (hostsFileName)
    localhost = "localhost"
    hosts = Set()
    hosts.add(localhost)
    resolv = dns.resolver.Resolver()
    for line in hostsFile.read().split("\n"):
        if (len(line)>1):
            if (line[0]!="#"):
                if (len(line.split())>1):
                    host = line.split()[1]
                    print "Checking "+host+" ..."
                    try:
                        resolve = resolv.query(host)
                        hosts.add(host)
                        print "OK"
                    except dns.resolver.NXDOMAIN:
                        print "Resolve error: NXDOMAIN"
                        pass
                    except dns.resolver.dns.resolver.NoNameservers:
                        print "Resolve error: NoNameServers"
                        pass
                    except dns.resolver.dns.resolver.NoAnswer:
                        print "Resolve error: NoAnswer"
                        pass
                    except dns.exception.Timeout:
                        print "Resolve error: Timeout"
                        pass
#                     print host
    
    thinnedHostsFile = open(hostsFileName+"_thinned", "w")
    thinnedHostsFile.write("".join(["127.0.0.1    "+line + "\n" for line in hosts]))
    thinnedHostsFile.close()
    print "Thinned hosts file written to "+hostsFileName
    print "Bye!"
        
    

if __name__ == '__main__':
    checkHosts(sys.argv[1])
